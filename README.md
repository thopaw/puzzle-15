# puzzle-15

Basic implementation of [puzzle 15](https://en.wikipedia.org/wiki/15_puzzle).

This was a small example to learn [Sveltejs](https://svelte.dev/) and [tailwindcss](https://tailwindcss.com/).

## Setup

You have to install dependencies with

```bash
npm install
```

## Build

You can run it with

```bash
npm run dev
```

## Deploy

The Deployment is done with [gitlab-ci](./gitlab-ci.yml) and hosted as [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/).

## Demo

https://thopaw.gitlab.io/puzzle-15/
