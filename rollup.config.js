import svelte from "rollup-plugin-svelte";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import pluginJson from "@rollup/plugin-json";
import livereload from "rollup-plugin-livereload";
import { terser } from "rollup-plugin-terser";
import sveltePreprocess from "svelte-preprocess";
import typescript from "@rollup/plugin-typescript";
import { generateSW } from "rollup-plugin-workbox";
import manifestJson from "rollup-plugin-manifest-json";

import packageInfo from "./package.json";

const production = !process.env.ROLLUP_WATCH;

function serve() {
  let server;

  function toExit() {
    if (server) server.kill(0);
  }

  return {
    writeBundle() {
      if (server) return;
      server = require("child_process").spawn(
        "npm",
        ["run", "start", "--", "--dev"],
        {
          stdio: ["ignore", "inherit", "inherit"],
          shell: true,
        }
      );

      process.on("SIGTERM", toExit);
      process.on("exit", toExit);
    },
  };
}

export default {
  input: "src/main.ts",
  output: {
    sourcemap: true,
    format: "iife",
    name: "app",
    file: "public/build/bundle.js",
  },
  plugins: [
    pluginJson(),
    svelte({
      // enable run-time checks when not in production
      dev: !production,
      // we'll extract any component CSS out into
      // a separate file - better for performance
      css: (css) => {
        css.write("bundle.css");
      },
      preprocess: sveltePreprocess({ postcss: true }),
    }),

    resolve({
      browser: true,
      dedupe: ["svelte"],
    }),
    commonjs(),
    typescript({ sourceMap: !production }),

    !production && serve(),
    !production && livereload("public"),
    production && terser(),

    // Service Worker
    generateSW({
      mode: production ? "production" : undefined,
      swDest: "public/sw.js",
      inlineWorkboxRuntime: true,
      sourcemap: !production,
      globDirectory: "public/",
    }),
    manifestJson({
      input: "src/manifest.json",
      minify: true,
      manifest: {
        short_name: packageInfo.name,
        name: packageInfo.name,
        author: packageInfo.author.name,
        description: packageInfo.description,
        homepage_url: packageInfo.homepage,
        start_url: "../",
        display: "standalone",
        theme_color: "#4299e1",
        background_color: "#bee3f8",
      },
    }),
  ],
  watch: {
    clearScreen: true,
  },
};
