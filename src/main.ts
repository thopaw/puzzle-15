import App from "./App.svelte";

// Register Service Worker
if ("serviceWorker" in navigator) {
  console.log("try to register service-worker");

  navigator.serviceWorker
    .register("./sw.js")
    .then(function (registration) {
      console.log("Registration successful, scope is:", registration.scope);
    })
    .catch(function (error) {
      console.log("Service worker registration failed, error:", error);
    });
}

const app = new App({
  target: document.body,
  props: {
    showSolveButon: false,
  },
});

export default app;
