import { writable, readable, derived, Readable } from "svelte/store";
import intervalToDuration from "date-fns/intervalToDuration";
import type { Interval } from "date-fns";

export const moves = writable<number>(0);
export const solvingStartTime = writable<Date>(undefined);

const time = readable<Date>(new Date(), function start(set) {
  const interval = setInterval(() => {
    set(new Date());
  }, 1000);

  return function stop() {
    clearInterval(interval);
  };
});

export const interval = derived<
  [Readable<Date | undefined>, Readable<Date>],
  Interval | undefined
>([solvingStartTime, time], ([$solvingStartTime, $time], set) => {
  if ($solvingStartTime) {
    set({ start: $solvingStartTime, end: $time });
  } else {
    set(undefined);
  }
});

export const solvingDuration = derived(interval, ($interval, set) => {
  if ($interval) {
    set(intervalToDuration($interval));
  } else {
    set(undefined);
  }
});
